import './App.css';
import GlobalFonts from './assets/fonts/fonts';
import Theme from './styles/Theme';
import Login from './Components/Login/Login';
import { AuthProvider } from './contexts/AuthContext';
import PrivateRoute from './Routes/PrivateRoute';
import {
  BrowserRouter as Router, Switch, Route
} from "react-router-dom";

function App() {

  return (
      <Theme>
          <GlobalFonts/>
          <Router>
            <AuthProvider>
              <Switch>
                <PrivateRoute 
                  exact 
                  path="/" 
                  />
                <Route 
                  path="/login" 
                  component={Login}
                  exact/>
              </Switch>
            </AuthProvider>
          </Router>
      </Theme>
  );
}

export default App;