import React, { useCallback, useContext, useEffect, useState } from 'react';
import {auth} from '../firebase';
import FilesBoard from '../Components/FilesBoard/FilesBoard';
import DashBoard from '../Components/Dashboard/Dashboard';
import {Redirect} from  'react-router-dom';

const AuthContext = React.createContext();

export const useAuth = ()=>{
    return useContext(AuthContext)
}

export const AuthProvider = ({children})=>{

    const [currentUser,setCurrentUser] = useState(null);

    const login = (email,password)=>{
        return auth.signInWithEmailAndPassword(email,password);
    }


    const getUser = useCallback(()=>{
        switch(JSON.parse(sessionStorage.getItem('user'))[0].role){
            case 'Admin':
                return <FilesBoard />
            case 'User':
                return <DashBoard/>
            default:
                <Redirect to="/login" />  
        }
        // return userData
    },[]);
    const value = {
        login,
        currentUser,
        getUser
    }
    useEffect(()=>{
        const unsubscribe = auth.onAuthStateChanged(user=>{
            setCurrentUser(user)
        })
        return unsubscribe;
            
    },[])

    return  (
        <AuthContext.Provider value={value}>
            {children}
        </AuthContext.Provider>
    )

}