import styled,{css} from 'styled-components';

const Input = styled.input`
    background-color:transparent;
    border:1px solid #ccc;
    &:focus{
        outline:none;
        border:1px solid;
        border-color:${({theme:{colors}})=>colors.mainColor};
    }
    width:${props=>props.width};
    font-size:${({theme:{fontSize}})=>fontSize.text};
    border-radius:${({theme:{border}})=>border.radius};
    font-family:'MontserratRegular';
    font-size:${({theme:{fontSize}})=>fontSize.text};
    padding:10px;
    margin-bottom:20px;
    height:50px;
    ${props => !props.isEmptyField && props.error && css`
        border-color:${({theme:{colors}})=>colors.errorRed};
    `}
`

export default Input;