const size = {
    xs:"550px",
    sm:"768px",
    slg:"990px",
    lg:"1024px",
    xl:"1200px",
  }

export const Device = {
    mobile:`(max-width:${size.xs})`,
    tablet:`(max-width:${size.sm})`,
    medTablet:`(max-width:${size.slg})`,
    bigTablet:`(max-width:${size.lg})`,
    desktop:`(max-width:${size.xl})`,
  }

