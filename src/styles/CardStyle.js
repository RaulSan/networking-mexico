import styled,{css} from 'styled-components';
import {Device} from '../styles/Breakpoints';

export const CardStyle = styled.div`
    display:flex;
    justify-content:space-between;
    position:relative;
    border-radius:${({theme:{border}})=>border.radius};
    background-color:${({theme:{colors}})=>colors.white};
    box-shadow:0 2px 4px #ccc;
    height:200px;
    width:100%;
    padding:20px;
    overflow:hidden;
    .infoWrapper{
        display:flex;
        flex-direction:column;
        width:100%;
        height:100%;
        cursor: pointer;
    }
    .infoProfile{
        display:flex;
        &-wrapper{
            margin-left:20px;
        }
        p{
            margin-bottom:5px;
            -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        -webkit-tap-highlight-color: transparent;
        }
        &-contact{
            display:flex;
            justify-content:flex-start;
            align-items:center;
            &:not(:last-child){
                margin-bottom:5px;
            }
            & svg{
                width:25px;
                height:25px;
                & > *{
                    fill:${({theme:{colors}}) => colors.mainColor};
                    & > *{
                    fill:${({theme:{colors}}) => colors.mainColor};
                }
                }
            }
        }
        &-name{
            font-family:${({theme:{font}}) => font[1]} ;
            font-size:${({theme:{fontSize}}) => fontSize.subtitle};
        }
        &-company , &-area{
            font-family:${({theme:{font}}) => font[1]} ;
            font-size:${({theme:{fontSize}}) => fontSize.text};
            color:${({theme:{colors}}) => colors.grey};

        }
        &-area{
            margin-bottom:25px !important;
        }
        &-email, &-tel{
            font-family:${({theme:{font}}) => font[1]} ;
            font-size:${({theme:{fontSize}}) => fontSize.textDetail};
            color:${({theme:{colors}}) => colors.mainColor};
            margin-bottom:0!important;
            margin-left:10px;
        }
    }
    @media only screen and ${Device.mobile}{
        height:200px;
    };
`

export const ProfileImg = styled.div`
    border-radius:100%;
    width:${props => props.width ? props.width : '70px'};
    min-width:${props => props.minWidth ? props.minWidth : '70px'};
    max-width:${props => props.maxWidth ? props.maxWidth : '70px'};
    height:${props => props.height ? props.height : '70px'};
    ${props => props.img && css`
        background-image:url(${props.img});
    `}
    background-position:center;
    background-repeat:no-repeat;
    background-size:cover;

`