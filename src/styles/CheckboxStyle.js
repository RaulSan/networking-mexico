import styled from 'styled-components';

export const CheckboxStyle = styled.div`
    width:25px;
    min-width:25px;
    height:25px;
    display:flex;
    justify-content:center;
    align-items:center;
    input{
        display:none;
    }
    input:checked + label{
        background-color:${({theme:{colors}})=>colors.mainColor};
    }
    input:checked + label:before{
        opacity:1;
    }
    label{
        position:relative;
        width:100%;
        height:100%;
        border:2px solid ${({theme:{colors}})=>colors.mainColor};
        border-radius:7px 7px 7px 0;
        cursor:pointer;
        transition: background-color 300ms;
        &:before{
            content:'';
            position:absolute;
            top:calc(50% - 6px);
            left:calc(50% - 7px);
            width:12px;
            height:5px;
            opacity:0;
            border-bottom:2px solid ${({theme:{colors}})=>colors.white};
            border-left:2px solid ${({theme:{colors}})=>colors.white};
            transform:rotate(-45deg);
            transition: opacity 300ms;
        }
    }
`