import styled from 'styled-components';
import {Device} from '../styles/Breakpoints';

export const SearchBarStyle = styled.div`
    background-color:transparent;
    border:none;
    min-width:230px;
    height: 50px;
    width:${props=>props.width};
    border-radius:${({theme:{border}})=>border.radius};
    font-family:'MontserratRegular';
    font-size:${({theme:{fontSize}})=>fontSize.text};
    position: relative;
    display:flex;
    flex-grow:1;
    align-items:center;
    > svg{
        width:30px;
        height:30px;
        position:absolute;
        transform:translate(0,-50%);
        left:5px;
        top:50%;
         > *{
            stroke:#ccc;
        }
    }
    @media only screen and ${Device.medTablet}{
        flex-grow:0;
    };
`

export const SearchBarInput = styled.input`
    min-width:230px;
    width:${props=>props.width};
    height:100%;
    padding:10px 10px 10px 40px;
    border:1px solid #ccc;
    border-radius:${({theme:{border}})=>border.radius};
    outline:none;
    font-family:'MontserratRegular';
    font-size:${({theme:{fontSize}})=>fontSize.text};
    margin-right:10px;
    flex-grow:1;
    &:focus{
        outline:none;
        border:1px solid;
        border-color:${({theme:{colors}})=>colors.mainColor};
    }
`