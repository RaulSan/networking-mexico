import styled,{css} from 'styled-components';
import {Device} from '../styles/Breakpoints';

export const SidebarStyle = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
    pointer-events: none;
    z-index: 150;
    transition: all 300ms linear;
    ${props=>props.isFilterOpen && css`
        backdrop-filter: blur(2px);
        background-color: rgba(255,255,255,.5);
        pointer-events:auto;
    `}
   
        .sideBar{
            padding:50px;
            z-index: 160;
            pointer-events: auto;  
            background-color:${({theme:{colors}})=>colors.secondColor};
            height: 100vh;
            position:relative;
            width:350px;
            transform:${props=>props.isFilterOpen ? "translateX(0)" : "translateX(-100%)"};
            transition: transform 300ms linear;
            will-change: transform;
            .crossIcon{
                width:25px;
                height:25px;
                position:absolute;
                right:20px;
                top:12px;
                cursor:pointer;
                
                svg > *{
                    fill:${({theme:{colors}})=>colors.black};
                }
            }
            .title{
                font-family:${({theme:{font}})=>font[0]};
                font-size:${({theme:{fontSize}})=>fontSize.title};
                color:${({theme:{colors}})=>colors.black};
                margin-bottom:40px;
            }
            .checkbox-wrapper{
                display:flex;
                margin-bottom:10px;
               & p{
                    font-family:${({theme:{font}})=>font[1]};
                    font-size:${({theme:{fontSize}})=>fontSize.text};
                    color:${({theme:{colors}})=>colors.black};
                    display:flex;
                    align-items:center;
                    margin-left:20px;
                }
            }
            @media only screen and ${Device.mobile}{
                    width:100%;
            };
           
        }
    

`