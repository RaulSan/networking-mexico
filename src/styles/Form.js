import styled from 'styled-components';
import {Device} from '../styles/Breakpoints';

const Form = styled.form`
    display:flex;
    align-items:center;
    flex-direction:column;
    width:70%;
    p{
        width:100%;
        color:${({theme:{colors}})=>colors.black};
        font-size:${({theme:{fontSize}})=>fontSize.bigTitle};
        text-align:left;
        font-family: 'MontserratBold';
        margin-bottom:20px;
        @media only screen and ${Device.tablet}{
        font-size:${({theme:{fontSize}})=>fontSize.title};
    };
    }
    @media only screen and ${Device.mobile}{
        width:85%;
    };
    
`
export default Form;