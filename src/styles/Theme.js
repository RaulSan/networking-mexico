import React from "react";
import { ThemeProvider } from "styled-components";

const theme = {
  colors: {
    mainColor: "#5E81FF",
    secondColor: "#D3DDFF",
    black: "#000000",
    white: "#FFFFFF",
    grey: "#CCCCCC",
    errorRed:"#ff3333"
  },
  font:['MontserratBold','MontserratSemiBold','MontserratRegular'],
  fontSize: {
    bigTitle:"3.5rem",
    title: "2.5rem",
    subtitle: "1.8rem",
    text: "1.6rem",
    textDetail:"1.4rem"
  },
  border:{
      radius:"8px"
  },
};

const Theme = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default Theme;