import React, { useEffect, useState } from 'react';
import styled,{keyframes} from 'styled-components';
import Card from './Card/Card';
import {ReactComponent as EmptyImg} from '../../../assets/dashboard/empty.svg';
import {Device} from '../../../styles/Breakpoints';

const CardsWrapperStyled = styled.div`
    width:100%;
    display:grid;
    margin-bottom:50px;
    grid-gap:40px;
    grid-auto-rows: 1fr;
    grid-template-columns: repeat( auto-fit, minmax(350px, 1fr) );
    @media only screen and ${Device.mobile}{
        grid-template-columns: repeat( auto-fit, minmax(250px, 1fr) );
        grid-gap:20px;
    };
`
const EmptyMessageStyle = styled.div`
    display:flex;
    flex-direction:column;
    align-items:center;
    p{
        font-family:${({theme:{font}})=>font[1]};
        font-size:${({theme:{fontSize}})=>fontSize.bigTitle};
        color:${({theme:{colors}})=>colors.black};
        text-align:center;
        margin-bottom:50px;
        @media only screen and ${Device.mobile}{
            font-size:${({theme:{fontSize}})=>fontSize.title};
        }
    }
    svg{
        width:550px;
        height:550px;
        @media only screen and ${Device.mobile}{
            width:250px;
            height:250px;
        }
    }
`
const skeleton = keyframes`
    from {
        left: -150px;
    }
    to   {
        left: 100%;
    }
`


const CardLoader = styled.div`
    position:relative;
    border-radius:${({theme:{border}})=>border.radius};
    background-color:#b7b3b321;
    box-shadow:0 2px 4px #ccc;
    height:200px;
    width:100%;
    padding:20px;
    overflow:hidden;
    &:before {
    content: '';
    display: block;
    position: absolute;
    left: -150px;
    top: 0;
    height: 100%;
    width: 100%;
    background: linear-gradient(to right, transparent 0%, white 50%, transparent 100%);
    animation: ${skeleton} 2.5s cubic-bezier(0.4, 0.0, 0.2, 1) infinite;
}`



const EmptyMessage = ()=>{
    return (
        <EmptyMessageStyle>
            <p>No hay resultados</p>
            <EmptyImg/>
        </EmptyMessageStyle>

    )
}

const CardsWrapper = ({loading,users,searchBar})=>{

    const loadingCards = Array(7).fill(0);
    const [userFilter,setUserFilter] = useState([]);

    const removeAccents = (str) => {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      } 


    useEffect(()=>{
        const usersFiltered = users.filter((val)=>{
            if(searchBar === ""){
                return val
            }else if(
                removeAccents(val.name.toLowerCase()).includes(removeAccents(searchBar.toLowerCase())) ||
                removeAccents(val.lastName.toLowerCase()).includes(removeAccents(searchBar.toLowerCase())) ||
                removeAccents(val.email.toLowerCase()).includes(removeAccents(searchBar.toLowerCase())) ||
                removeAccents(val.phone.toLowerCase()).includes(removeAccents(searchBar.toLowerCase())) ||
                removeAccents(val.company.toLowerCase()).includes(removeAccents(searchBar.toLowerCase())) ||
                removeAccents(val.description.toLowerCase()).includes(removeAccents(searchBar.toLowerCase())) ||
                removeAccents(val.area.toLowerCase()).includes(removeAccents(searchBar.toLowerCase()))
                ){
                return val
            } 
            return false;
        })
        
        setUserFilter(usersFiltered);
    },[setUserFilter,searchBar,users])

    return(
        <CardsWrapperStyled>
            {loading ?
                loadingCards.map((_,index)=>{
                    return (
                        <CardLoader key={index}/>
                    )  
                })
		:	    
		 userFilter.map( ( user, index ) => {
			 return (
				       user.role !== 'Admin' ?
					 < Card key={index} user={user} />
					 : null
				 )
            })
            }

            {!loading && (users.length === 0 || userFilter.length === 0) &&
                <EmptyMessage />
            }
        
        </CardsWrapperStyled>
    )
}

export default CardsWrapper;