import React, { useState } from 'react';
import {CardStyle,ProfileImg} from '../../../../styles/CardStyle';
import {ButtonCard} from '../../../Shared/Button/Button';
import {ReactComponent as EmailIcon} from '../../../../assets/dashboard/email.svg';
import {ReactComponent as PhoneIcon} from '../../../../assets/dashboard/phone.svg';
import {ReactComponent as InfoIcon} from '../../../../assets/dashboard/info.svg';
import CardToolTip from './CardTooltip/CardToolTip';

const Card = ({user})=>{

    const [companyInfo,setCompanyInfo] = useState(false);


    const toggleCompanyInfo = ()=>{
        setCompanyInfo(true)
    }

    const getName = (name,lastName)=>{
        if(name && lastName){
        const familyName = lastName.split(' ')[0]
        return name + " " +  familyName
    }
    }

    return (
        <CardStyle>
            <div onClick={toggleCompanyInfo} className="infoWrapper">
                <div className="infoProfile">
                        <ProfileImg img={user.profileImg}>
                        </ProfileImg>
                        <div className="infoProfile-wrapper">
                            <p className="infoProfile-name">{getName(user.name,user.lastName)}</p>
                            <p className="infoProfile-company">{user.company}</p>
                            <p className="infoProfile-area">{user.description}</p>
                        </div>
                </div>
                <div>
                    <div className="infoProfile-contact">
                        <EmailIcon/>
                        <p className="infoProfile-email">{user.email}</p>
                    </div>
                    <div className="infoProfile-contact">
                        <PhoneIcon/>
                        <p className="infoProfile-tel">{user.phone}</p>
                    </div>
                </div>
            </div>
            <ButtonCard onClick={toggleCompanyInfo}><InfoIcon/></ButtonCard>
            <CardToolTip area={user.area} logoImg={user.logoImg} isCompanyInfoOpen={companyInfo} setCompanyInfo={setCompanyInfo}></CardToolTip>
        </CardStyle>
    )
}

export default Card;