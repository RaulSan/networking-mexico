import React from 'react';
import styled from 'styled-components';
import {ProfileImg} from '../../../../../styles/CardStyle';
import {ReactComponent as CrossIcon} from '../../../../../assets/dashboard/cross.svg';

const CardTootlTipStyle = styled.div`
    width:100%;
    height:100%;
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    border-radius:${({theme:{border}})=>border.radius};
    background-color:${({theme:{colors}})=>colors.white};
    padding:20px;
    display:flex;
    z-index:10;
    transition:transform 300ms linear;
    transform:${props=>props.isCompanyInfoOpen ? "translateX(0)" : "translateX(-100%)"};
    will-change:transform;
    .area{
        margin-left:20px;
        p{
            font-family:${({theme:{font}}) => font[1]} ;
            font-size:${({theme:{fontSize}}) => fontSize.subtitle};
            color:${({theme:{colors}}) => colors.black};
            margin-bottom:10px;
        }
        &-text{
            font-family:${({theme:{font}}) => font[1]} ;
            font-size:${({theme:{fontSize}}) => fontSize.textDetail};
            color:${({theme:{colors}}) => colors.mainColor} !important;
        }
            
    }
    .crossIcon-tooltip{
        width:25px;
        height:25px;
        position:absolute;
        right:20px;
        top:12px;
        cursor:pointer;
        svg > *{
            fill:${({theme:{colors}})=>colors.black};
        }
    }
` 



const CardToolTip = ({isCompanyInfoOpen,setCompanyInfo,area,logoImg})=>{
    const closeCardToolTip = ()=>{
        setCompanyInfo(false)
    }
    return(
        <CardTootlTipStyle isCompanyInfoOpen={isCompanyInfoOpen}>
                <div onClick={closeCardToolTip} className="crossIcon-tooltip">
                    <CrossIcon/>
                </div>
                <ProfileImg width="70px" height="70px" img={logoImg}>
                </ProfileImg>
                <div className="area">
                    <p>Áreas</p>
                    <p className="area-text">{area}</p>
                </div>
        </CardTootlTipStyle>
    )
}

export default CardToolTip;
