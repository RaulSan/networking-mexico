import React,{useState,useEffect} from 'react';
import styled from 'styled-components';
import TopBar from '../../Components/Shared/TopBar/TopBar';
import CardsWrapper from '../../Components/Dashboard/CardsWrapper/CardsWrapper';
import {Device} from '../../styles/Breakpoints';
import Sidebar from '../Shared/Sidebar/Sidebar';
import { db } from '../../firebase';

const DashBoardStyled = styled.div`
    width:100vw;
    height:100vh;
    padding:0 50px 0 50px;
    margin-top:20px;
    padding-bottom:40px;
    @media only screen and ${Device.mobile}{
        padding:0 20px 0 20px;
    };
`

const DashBoard = ()=>{
    const [filter,setFilter] = useState(false);
    const [areaLabels,setAreaLabels] = useState([]);
    const [user,setUser] = useState([]);
    const [checkedStatus,setCheckedStatus] = useState([])
    const [loading,setLoading] = useState(true);

    const [searchBar, setSearchBar] = useState('');

    const getUsers = async()=>{
        const users = await db.collection('users').get();
        const filter = await db.collection('filtros').get();
        setLoading(false);
        const usersCollection = users.docs.map(doc=>{
            return {...doc.data(),id:doc.id};
        })
        const filterCollection = filter.docs.map(doc=>{
            return doc.data();
        })
        setUser(usersCollection)
        const filterInfo = filterCollection[0].areas.map((data)=>{
            return {label:data,checked:false}
        })
        setCheckedStatus(filterInfo)

    };

    const handleFilter = async()=>{
        const activeFilters = checkedStatus.filter((data)=>data.checked);
        const activeFiltersLabel = activeFilters.map(item=>item.label)
        if(activeFilters.length){
            const usersQuery = db.collection('users').where('area', 'in', activeFiltersLabel);
            const usersFilert = await usersQuery.get()
            const usersCollection = usersFilert.docs.map(doc=>{
                return {...doc.data(),id:doc.id};
            })
            setUser(usersCollection);
        }else{
            getUsers();
        }
        setAreaLabels(activeFilters);
        setFilter(false);
    }

    const closeFilter = ()=>{
        setFilter(false);
    }
    
    useEffect(()=>{
        getUsers();
        return sessionStorage.clear();
    },[])


    return (
        <DashBoardStyled>
            <TopBar setSearchBar={setSearchBar} searchBar={searchBar} areaLabels={areaLabels} filter={filter} setFilter={setFilter}/>
            <CardsWrapper loading={loading} users={user} searchBar={searchBar}/>
            <Sidebar 
                handleFilter={handleFilter}
                checkedStatus={checkedStatus} 
                setCheckedStatus={setCheckedStatus} 
                isFilterOpen={filter} 
                setFilter={setFilter}
                areaLabels={areaLabels}
                closeFilter={closeFilter}
            />
        </DashBoardStyled>
    )
}

export default DashBoard;