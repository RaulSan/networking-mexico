import React, { useEffect,useState,useRef } from 'react';
import styled,{css,keyframes} from 'styled-components';
import FolderImage from '../../assets/filesboard/folderIcon.svg';
import {Button} from '../../Components/Shared/Button/Button';
import DragAndDrop from '../Shared/DragAndDrop/DragAndDrop';
import {Device} from '../../styles/Breakpoints';
import FileLogo from '../../assets/filesboard/csvLogo.svg';
import {ReactComponent as CrossIcon} from '../../assets/filesboard/crossIcon.svg';
import {ReactComponent as CheckIcon} from '../../assets/filesboard/checkIcon.svg';
import {db} from '../../firebase';

const FileBoardStyle = styled.div`
display:flex;
justify-content:center;
align-items:center;
background-color:#F5F8FF;
width:100vw;
height:100vh;
`
const FileContainer = styled.div `
display:flex;
flex-direction:column;
align-items:center;
background-color:${({theme:{colors}})=>colors.white};
box-shadow:0 4px 4px 0px #e9e9e9;
border-radius:20px;
width:50%;
padding:80px;
position:relative;
#fileManager{
    display:none;
}
.title{
    font-family:'MontserratSemiBold';
    font-size:${({theme:{fontSize}})=>fontSize.bigTitle};
    text-align:center;
    margin-bottom:10px;
}
.subTitle{
    font-family:'MontserratSemiBold';
    font-size:${({theme:{fontSize}})=>fontSize.text};
    color:${({theme:{colors}})=>colors.grey};
    text-align:center;
    margin-bottom:100px;
    @media only screen and ${Device.mobile}{
        margin-bottom:40px;
    };
}
@media only screen and ${Device.mobile}{
    padding:40px 20px;
};
@media only screen and ${Device.desktop}{
    width:90%;
};
`


const FileUpload = styled.div`
display:flex;
flex-direction:column;
justify-content:center;
align-items:center;
width:100%;
border-radius:20px;
border:2px dashed ${({theme:{colors}})=>colors.mainColor};
height:100%;
margin-bottom:20px;
background-color:#F5F8FF;
position: relative;
.fileManager-wrapper{
    background-color:transparent;
    cursor: pointer;
    position: absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    width:100%;
    height:100%;
}
.img-subtitle{
    font-family:'MontserratSemiBold';
    font-size:${({theme:{fontSize}})=>fontSize.text};
    color:${({theme:{colors}})=>colors.grey};
    text-align:center;
    margin-top:10px;
}
`
const FolderIcon = styled.div`
 ${props => props.img && css`
background-image:url(${props.img});
background-repeat:no-repeat;
background-size:contain;
width:100px;
height:100px;
`}
`

const DepositFiles = styled.div`
    display:flex;
    align-items:center;
    width:100%;
    margin-bottom:${props => props.margin ? props.margin : '20px'};
    border-radius:8px;
    padding:10px;
    box-shadow:0 4px 2px rgba(0,0,0,.1);
    position: relative;
    transition:opacity .8s;
    opacity:${props => props.fileLoaded ? 1 : 0};
      .crossIcon-wrapper{
        width:50px;
        height:50px;
        position:absolute;
        right:10px;
        cursor:pointer;
        @media only screen and ${Device.mobile}{
            width:30px;
            height:30px;
            right:5px;
        };
        svg{
        width:50px;
        height:50px;
        position:absolute;
        right:10px;
        cursor:pointer;
        @media only screen and ${Device.mobile}{
            width:30px;
            height:30px;
            right:5px;
        };
    }
    }
  
    img{
        width:40px;
        height:40px;
    }
    .fileLogo-text{
            font-family:'MontserratSemiBold';
            font-size:${({theme:{fontSize}})=>fontSize.textDetail};
            color:${({theme:{colors}})=>colors.grey};
            width:300px;
            text-overflow:ellipsis;
            white-space: nowrap;
            overflow: hidden;
            @media only screen and ${Device.mobile}{
                width:150px;
            };
    }
    .fileLogo-error{
        font-family:'MontserratSemiBold';
            font-size:${({theme:{fontSize}})=>fontSize.text};
            color:${({theme:{colors}})=>colors.errorRed};
    }
`

const DepositFilesError = styled(DepositFiles)`
    opacity:${props => props.error ? 1 : 0};
`

const rotation = keyframes `
    from{
        transform:rotate(0);
    }
    to{
        transform:rotate(360deg);
    }
`

const Loader = styled.span`
    width: 30px;
    height: 30px;
    border-color: white;
    border-width: 4px;
    border-style: dotted;
    border-top: 1px solid transparent;
    border-radius: 100px;
    animation: ${rotation} 1s infinite linear;
`

const FilesBoard = ()=>{

    const [fileData,setFile] = useState(null);
    const [error,setError] = useState('');
    const [payloadData,setPayloadData] = useState(false);
    const [loaded, setLoaded] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [upLoad, setUpload] = useState(false);
    const uploadInputRef = useRef(null);

    useEffect(()=>{
        return sessionStorage.clear();
    })

    const deleteLoadedFile = ()=>{
        uploadInputRef.current.value = null
        setFile(null);
        setError('');
        setIsLoading(false);
        setLoaded(false);
        setPayloadData(false);
        setUpload(false);
    }

    const handleCvs = async()=>{ 
        setIsLoading(true)
        try{
            for(let i = 0;i < payloadData.length;i++){
                await db.collection("users").add({
                    area:payloadData[i].area,
                    company:payloadData[i].company,
                    description:payloadData[i].description,
                    email:payloadData[i].email,
                    lastName:payloadData[i].lastName,
                    logoImg:payloadData[i].logoImg,
                    name:payloadData[i].name,
                    phone:payloadData[i].phone,
                    profileImg:payloadData[i].profileImg,
                    role:payloadData[i].role
                }); 
            }   
            setIsLoading(false); 
            setUpload(true); 
            setTimeout(()=>{
                setFile(null);
                setError('');
                setIsLoading(false);
                setLoaded(false);
                setPayloadData(false);
                setUpload(false); 
            },1500)
        }
        catch(e){
            setFile(null);
            setError('');
            setIsLoading(false);
            setLoaded(false);
            setPayloadData(false);
            setUpload(false); 
        }
    }

    const csvJSON = (csv) =>{

        var lines=csv.split("\n");
        var result = [];
    
        var headers=lines[0].split(",");
      
        for(let i=1;i<lines.length;i++){
      
            var obj = {};
            var currentline=lines[i].split(",");
            for(let j=0;j<headers.length;j++){
        
                obj[headers[j].trim()] = currentline[j];
              
            }
      
            result.push(obj);
      
        }
        return JSON.stringify(result); 
      }


	const handleUploadFiles = ( file ) => {
        if(!fileData){
            if(file.length === 1 && file[0].type === 'text/csv'){
                    setError('')
                    let myFile = file[0];
                    let reader = new FileReader();
                    
                    reader.addEventListener('load', (e) => {
                        let csvdata = e.target.result; 
                        const payload = JSON.parse(csvJSON(csvdata));
                        setPayloadData(payload);
                        setLoaded(true);
                    });
                    reader.readAsText(myFile);
                    setFile(file[0])
                }else{
                    uploadInputRef.current.value = null
                    setError('Archivos no permitidos')
                    setTimeout(()=>{
                        setError('')
                    },4000)
                } 
        }else{
            setError('Ya hay un archivo cargado')
            setTimeout(()=>{
                setError('')
            },4000);
        }
    }

    return (
        <FileBoardStyle>
            <FileContainer>
                <p className="title">Subir Archivo</p>
                <p className="subTitle">El acrhivo debe ser .csv</p>
                <DragAndDrop handleUploadFiles={handleUploadFiles}>
                    <FileUpload>
                        <label className="fileManager-wrapper" htmlFor="fileManager"></label>
                        <FolderIcon img={FolderImage}/>
                        <p className="img-subtitle">Arrastra aqui tu archivo</p>
                    </FileUpload>
                </DragAndDrop>
                {fileData &&
                    <DepositFiles fileLoaded={loaded}>
                        <img src={FileLogo} alt="file"/>
                        <p className="fileLogo-text">{fileData.name}</p>
                        <div onClick={deleteLoadedFile} className="crossIcon-wrapper">
                            {upLoad ?
                                <CheckIcon/>
                            :
                                <CrossIcon/>
                            }
                        </div>
                    </DepositFiles>
                }
                
                    <DepositFilesError error={error}>
                    <p className="fileLogo-error">{error}</p>
                    </DepositFilesError>
                
                <Button width="100%" margin="20px 0 0 0" disabled={!loaded} isLoading={isLoading} onClick={handleCvs}>{!isLoading ? 'Subir archivo': <Loader />}</Button>
                <input ref={uploadInputRef} onChange={(e)=>handleUploadFiles(e.target.files)}  id="fileManager" type="file"/>
            </FileContainer>
        </FileBoardStyle>
    )
};

export default FilesBoard;