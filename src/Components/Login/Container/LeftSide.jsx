import React from 'react';
import {ReactComponent as OfficeSvg} from '../../../assets/login/office.svg'
import styled from 'styled-components';
import {Device} from '../../../styles/Breakpoints';

const Side = styled.div`
    width:50%;
    height:100vh;
    background-color:${({theme:{colors}}) => colors.secondColor};
    display:flex;
    justify-content:center;
    align-items:center;
    > svg{
        width:80%;
        height:auto;
    }
    @media only screen and ${Device.medTablet}{
        width:100%;
        background-color:${({theme:{colors}}) => colors.white};
        height:50vh;
        align-items:flex-end;
    };
    @media only screen and ${Device.mobile}{
        align-items:center;
    };
`
const Logo = styled.p`
    font-family:'MontserratBold';
    font-size:${({theme:{fontSize}}) => fontSize.bigTitle} ;
    position:absolute;
    top:150px;
    transform:translate(-50%,0);
    left:50%;
    display:none;
    span{
        font-family:'MontserratRegular';
        font-size:${({theme:{fontSize}}) => fontSize.title};
        margin-left:5px;
     }  
     @media only screen and ${Device.medTablet}{
        display:block;
        top:50px;
    };
    @media only screen and ${Device.mobile}{
        font-size:${({theme:{fontSize}}) => fontSize.title} ;
        span{
            font-size:${({theme:{fontSize}}) => fontSize.subtitle};
        }
    };
`

const LeftSide = ()=>{
    return (
        <Side>
          <Logo>NETWORKING<span>MÉXICO</span></Logo>
          <OfficeSvg /> 
        </Side> 
    )
}

export default LeftSide;