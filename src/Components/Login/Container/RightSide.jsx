import React from 'react';
import styled,{keyframes} from 'styled-components';
import Input from '../../../styles/Input';
import Form from '../../../styles/Form';
import {Button} from '../../Shared/Button/Button';
import {Device} from '../../../styles/Breakpoints';



const Side = styled.div`
    width:50%;
    height:100vh;
    background-color:${({theme:{colors}}) => colors.white};
    position:relative;
    display:flex;
    justify-content:center;
    align-items:center;
    @media only screen and ${Device.medTablet}{
        width:100%;
        height:50vh;
    };
    @media only screen and ${Device.mobile}{
        align-items:flex-start;
    };
`
const Logo = styled.p`
    font-family:'MontserratBold';
    font-size:${({theme:{fontSize}}) => fontSize.bigTitle} ;
    position:absolute;
    top:150px;
    transform:translate(-50%,0);
    left:50%;
    span{
        font-family:'MontserratRegular';
        font-size:${({theme:{fontSize}}) => fontSize.title};
        margin-left:5px;
     }  
     @media only screen and ${Device.medTablet}{
        display:none;
    };
`

const ErrorMessage = styled.p`
    text-align:center !important;
    font-size:${({theme:{fontSize}}) => fontSize.textDetail} !important;
    color:${({theme:{colors}}) => colors.errorRed} !important;
`
const rotation = keyframes `
    from{
        transform:rotate(0);
    }
    to{
        transform:rotate(360deg);
    }
`

const Loader = styled.span`
    width: 30px;
    height: 30px;
    border-color: white;
    border-width: 4px;
    border-style: dotted;
    border-top: 1px solid transparent;
    border-radius: 100px;
    animation: ${rotation} 1s infinite linear;
`


const RightSide = ({handleLogin,handleSubmit,form,error,loading})=>{


    return (
        <Side>
            <Logo>NETWORKING<span>MÉXICO</span></Logo>
          <Form>
            <p>Ingresa</p>
            <Input error={error} isEmptyField={form.email} value={form.email || ''} name='email' onChange={handleLogin} width="100%" placeholder="Usuario ó Correo"/> 
            <Input error={error} isEmptyField={form.password} value={form.password || ''} name='password' onChange={handleLogin} type="password" width="100%" placeholder="Contraseña"/> 
            <ErrorMessage>{error}</ErrorMessage>
            <Button 
                disabled={loading} 
                isLoading={loading} 
                onClick={handleSubmit} 
                width="100%">{!loading ? 'Ingresa': <Loader />}</Button>
          </Form>  
        </Side>
    )
}

export default RightSide;