import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import RightSide from './Container/RightSide';
import LeftSide from './Container/LeftSide';
import {Device} from '../../styles/Breakpoints';
import {useAuth} from '../../contexts/AuthContext';
import {db} from '../../firebase';
import { useHistory } from "react-router-dom";;

const Container = styled.div`
    width:100vw;
    height:100vh;
    background-color:${({theme:{colors}}) => colors.secondColor};
    display:flex;
    @media only screen and ${Device.medTablet}{
        flex-direction:column;
    };
`


const Login = ()=>{

    const [form,setForm] = useState({});
    const [error,setError] = useState("");
    const [loading,setLoading] = useState(false);
    const { login } = useAuth();
    const history = useHistory();
    const isMounted = useRef(null);

    const handleSubmit = async(e)=>{
        isMounted.current = true;
            e.preventDefault();
            if(isMounted.current){
            setError("");
            setLoading(true);
            try{
                    if(form.email && form.password){
                        const userLogged = await login(form.email,form.password);
                        const userQuery =  db.collection('users').where('email','==',userLogged.user.email)

                        if(userQuery){  
                           
                            const userInfo = await userQuery.get()
                           
                            const userData = userInfo.docs.map(doc=>{
                                return {...doc.data(),id:doc.id};
                            })
                            sessionStorage.setItem('user',JSON.stringify(userData))
                            history.push("/");
                        }
                     }else{
                        setError("Campos requeridos")
                     }
            }catch(e){
                setError("Usuario o contraseña incorrecto");  
            }
            setLoading(false);
        }
    }
    useEffect(()=>{
        return ()=> isMounted.current = false
    })

    const handleLogin =(e)=>{
       const {name, value} = e.target
       setForm({...form,[name]:value})
    }

    return (
        <Container>
            <LeftSide />
            <RightSide
                loading={loading} 
                error={error} 
                form={form}
                handleSubmit={handleSubmit} 
                handleLogin={handleLogin}/>
        </Container>
    )
}

export default Login;