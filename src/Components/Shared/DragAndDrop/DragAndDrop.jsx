import React,{useCallback, useEffect, useRef, useState} from 'react';
import styled from 'styled-components';
import {Device} from '../../../styles/Breakpoints';


const DragOverlay = styled.div`
       position:absolute;
       top:0;
       bottom:0;
       left:0;
       right:0;
       background-color: rgba(255,255,255,.8);
       z-index:1000;
       display:flex;
       align-items:center;
       justify-content:center;
       .dragAndDrop-title{
            font-family:'MontserratSemiBold';
            font-size:${({theme:{fontSize}})=>fontSize.title};
            color:${({theme:{colors}})=>colors.grey};
            text-align:center;
       }
`
const DragAndDropStyles = styled.div`
        height:350px;
        width:100%;
        margin-bottom:20px;
        position:relative;
        @media only screen and ${Device.mobile}{
            height:250px;
    };
`



const DragAndDrop = ({handleUploadFiles,children})=>{

    const dropRef = useRef(null);
    const [dragging,setDragging] = useState(false);
    let [dragCounter,setDragCounter] = useState(0);

    const handleDrag = useCallback((e) => {
        e.preventDefault()
        e.stopPropagation()
      },[])

    const handleDragIn = useCallback((e) => {
    e.preventDefault()
    e.stopPropagation()
    setDragCounter(dragCounter++)
    if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
        setDragging(true)
    }
    },[dragCounter])

    const handleDragOut = useCallback((e) => {
    e.preventDefault()
    e.stopPropagation()
    setDragCounter(dragCounter--)
    if (dragCounter === 0) {
        setDragging(false)
    }
    },[dragCounter])

    const handleDropped = useCallback((e) => {
    e.preventDefault()
    e.stopPropagation()
    setDragging(false)
	    setDragCounter( 0 )	
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
         handleUploadFiles(e.dataTransfer.files)
	 setDragCounter(0)  
    }
    },[handleUploadFiles])

    useEffect(()=>{
        let {current} = dropRef
        current.addEventListener('dragenter', handleDragIn)
        current.addEventListener('dragleave', handleDragOut)
        current.addEventListener('dragover', handleDrag)
        current.addEventListener('drop', handleDropped)
        return ()=>{
            current.removeEventListener('dragenter', handleDragIn)
            current.removeEventListener('dragleave', handleDragOut)
            current.removeEventListener('dragover', handleDrag)
            current.removeEventListener('drop', handleDropped)
        }
    },[handleDragIn,handleDragOut,handleDrag,handleDropped]);

    return (
        <DragAndDropStyles
            ref={dropRef}
        >
            {dragging &&
                <DragOverlay>
                </DragOverlay>
            }
            {children}
        </DragAndDropStyles>
    )
}

export default DragAndDrop