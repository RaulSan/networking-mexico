import styled,{css,keyframes} from 'styled-components';

export const Button = styled.button`
    color:${({theme:{colors}})=>colors.white};
    background-color:${({theme:{colors}})=>colors.mainColor};
    border-radius:${({theme:{border}})=>border.radius};
    text-align:center;
    width:${props =>props.width};
    padding:10px;
    border:none;
    width:${props=>props.width};
    font-size:${({theme:{fontSize}})=>fontSize.subtitle};
    font-family:'MontserratSemiBold';
    cursor: pointer;
    box-shadow:0 4px 4px 0 #ccc;
    height:${props =>props.height ? props.height : "50px"};
    outline:none;
    transition:all 100ms;
    margin:${props =>props.margin ? props.margin : "0px"};
    ${props =>props.disabled && css`
        background-color:#a0a0a080;
    `}
    &:active{
        transform:translateY(-2px);
    }
    ${props =>props.isLoading && css`
        display:flex;
        justify-content:center;
        align-items:center;
        animation: ${btnLoading} .8s forwards;
    `}
`
const btnLoading = keyframes`
    0% {
        width: 100%;
    }
    50%   {
        width: 50px;
    }
    100%{
        width:50px;
        border-radius:50px;
    }
`

export const ButtonCard = styled(Button)`
    display:flex;
    justify-content:center;
    align-items:center;
    padding:5px;
    height:32px;
    width:32px;
    border-radius:100%;
    position:absolute;
    transform:translate(0,-50%);
    top:36px;
    right:20px;
    font-size:${({theme:{fontSize}})=>fontSize.text};
    &:active{
        transform:translateY(-20px);
    }
    & svg{
        width:40px;
        height:40px;
        & > *{
            fill:${({theme:{colors}})=>colors.white};
        }
    }
`