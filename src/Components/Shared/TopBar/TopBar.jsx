import React from 'react';
import SearchBar from '../SearchBar/SearchBar';
import styled from 'styled-components';
import {Device} from '../../../styles/Breakpoints';

const TopBarStyle = styled.div`
    width:100%;
    .topBar{
    display:flex;
    justify-content:space-between;
    margin-bottom:${props =>!!props.areaLabels.length ? '20px' : '80px'};
    @media only screen and ${Device.medTablet}{
        flex-direction:column;
    };
    @media only screen and ${Device.mobile}{
        margin-bottom:${props =>!!props.areaLabels.length ? '20px' : '40px'};
    };
    }
    .filter-label{
        font-family:${({theme:{font}}) => font[1]} ;
        font-size:${({theme:{fontSize}}) => fontSize.text};
        color:${({theme:{colors}}) => colors.grey};
        margin-bottom:10px;
    }
    .filter-selected{
        display:inline;
        font-family:${({theme:{font}}) => font[2]} ;
        font-size:${({theme:{fontSize}}) => fontSize.textDetail};
        color:${({theme:{colors}}) => colors.white};
        background-color:${({theme:{colors}}) => colors.mainColor};
        padding:5px 10px;
        border-radius:50px;
        margin-right:10px;
        margin-bottom:10px;
        @media only screen and ${Device.mobile}{
        font-size:12px;
    };
    }
    .filter-selected-wrapper{
        margin-bottom:20px;
        display:flex;
        flex-wrap:wrap;
    }
 
`

const Logo = styled.p`
    font-family:'MontserratBold';
    font-size:${({theme:{fontSize}}) => fontSize.bigTitle} ;
    margin-right:10px;
    text-align:center;
    display: flex;
    align-items: center;
    span{
        font-family:'MontserratRegular';
        font-size:${({theme:{fontSize}}) => fontSize.title};
        margin-left:5px;
    }
    @media only screen and ${Device.medTablet}{
        justify-content:center;
        margin-right:0px;
        margin-bottom:20px;
    };
    @media only screen and ${Device.desktop}{
        font-size:${({theme:{fontSize}}) => fontSize.title};
        span{
            font-size:${({theme:{fontSize}}) => fontSize.subtitle};
        }
    };
`   

    

const TopBar = ({setFilter,filter,areaLabels,searchBar,setSearchBar})=>{


    return(
        <TopBarStyle areaLabels={areaLabels}>
            <div className="topBar">
                <Logo>NETWORKING<span>MÉXICO</span></Logo>
                <SearchBar searchBar={searchBar} setSearchBar={setSearchBar} activeFilters={areaLabels} filter={filter} setFilter={setFilter}/>
            </div>
            {!!areaLabels.length &&
            <p className="filter-label">
                Filtros activos:
            </p>    
            }
            
            <div className="filter-selected-wrapper">
                {
                    areaLabels.map((area,index)=>{
                        return <div className="filter-selected" key={index}>{area.label}</div>
                    })
                }
            </div>
            
           

        </TopBarStyle>
    )
}

export default TopBar;