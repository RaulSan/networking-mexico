import React from 'react';
import {SidebarStyle} from '../../../styles/Sidebar';
import Checkbox from '../Components/Checkbox/Checkbox';
import {Button} from '../Button/Button';
import {ReactComponent as CrossIcon} from '../../../assets/dashboard/cross.svg';

const Sidebar = ({
    isFilterOpen,
    handleFilter,
    setCheckedStatus,
    checkedStatus,
    closeFilter
    })=>{


   


    return(
        <SidebarStyle
            onClick={closeFilter}
            isFilterOpen={isFilterOpen}
         >
            <div className="sideBar" onClick={(e)=>e.stopPropagation()}>
                <div onClick={closeFilter} className="crossIcon">
                    <CrossIcon/>
                </div>
                <p className="title">Filtros</p>
                {
                    checkedStatus.map((filter,index)=>{
                    return (
                        <div key={index} className="checkbox-wrapper">
                            <Checkbox
                                checkedStatus={checkedStatus} 
                                setCheckedStatus={setCheckedStatus} 
                                filter={filter} 
                                id={index} />
                            <p>{filter.label}</p>
                        </div>
                    )
                })
                    
                }
              <Button onClick={handleFilter} width="100%" height="40px" margin="40px 0 0 0">Aplicar</Button>
            </div>
        </SidebarStyle>
    )
}

export default Sidebar;