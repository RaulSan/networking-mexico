import React from 'react';
import {SearchBarStyle,SearchBarInput} from '../../../styles/SearchBarStyle';
import {ReactComponent as SearchIcon} from '../../../assets/dashboard/search.svg';
import {ReactComponent as FilterIcon} from '../../../assets/dashboard/filter.svg';
import styled from 'styled-components';

const Filter = styled.button`
    width:40px;
    height:40px;
    margin-left:10px;
    display:flex;
    align-items:center;
    justify-content:center;
    border:none;
    background-color:transparent;
    outline:none;
    cursor: pointer;
    > svg{
        width:40px;
        height:40px;
        > *{
            > *{
            fill:#ccc;
            }
        }
    }
`

const SearchBar = ({setFilter,filter,searchBar,setSearchBar})=>{

    const toggleFilter = ()=>{
        if(filter){
            setFilter(false)
        }else{
            setFilter(true)
        }
    }

    const handleInput = (e)=>{
        setSearchBar(e.target.value)
    }

    return(
        <SearchBarStyle>
            <SearchBarInput value={searchBar} onChange={handleInput} width="550px" placeholder="Buscar"/>
            <Filter onClick={toggleFilter}>
                <FilterIcon/>
            </Filter>
            <SearchIcon/>
        </SearchBarStyle>
    )
}

export default SearchBar;