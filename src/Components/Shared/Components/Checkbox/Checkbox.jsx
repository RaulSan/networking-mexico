import React from 'react';
import {CheckboxStyle} from '../../../../styles/CheckboxStyle';

const Checkbox = ({
    id,
    setCheckedStatus,
    checkedStatus,
    filter
})=>{


    const handleChecked = (e)=>{
        const copyChecked = checkedStatus;
        const matchCheck = copyChecked.findIndex((data)=> data.label === e.target.dataset.id)
        copyChecked[matchCheck] = {label:e.target.dataset.id,checked:e.target.checked}
        setCheckedStatus(copyChecked);
    }

    return (
        <CheckboxStyle>
            <input onChange={handleChecked} value={checkedStatus[id].checked}  data-id={filter.label} id={'check-' + id} type="checkbox"/>
            <label htmlFor={'check-' + id}></label>
        </CheckboxStyle>
    )
}

export default Checkbox;