import React from 'react';
import {Route, Redirect} from  'react-router-dom';
import {useAuth} from '../contexts/AuthContext';

const FilesRoute = ({component:Component,...rest}) =>{

    const {currentUser} = useAuth();


    return (
        <Route
            {...rest}
            render={props =>{
                return currentUser.role === "Admin" ? <Component {...props} /> : <Redirect to="/login" />
            }}
        ></Route>
    )


}

export default FilesRoute