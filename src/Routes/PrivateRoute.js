import React from 'react';
import {Route, Redirect} from  'react-router-dom';
import {useAuth} from '../contexts/AuthContext';

const PrivateRoute = ({...rest}) =>{

    const {currentUser,getUser} = useAuth();
  
    return (
        <Route
            {...rest}
            render={() =>{
                return currentUser ? getUser(currentUser) : <Redirect to="/login" />
            }}
        ></Route>
    )


}

export default PrivateRoute