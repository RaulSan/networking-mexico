import { createGlobalStyle } from 'styled-components';
import MontserratBold from './Montserrat/Montserrat-Bold.ttf';
import MontserratSemiBold from './Montserrat/Montserrat-SemiBold.ttf';
import MontserratRegular from './Montserrat/Montserrat-Regular.ttf';
import MontserratLight from './Montserrat/Montserrat-Light.ttf';

export default createGlobalStyle`
    @font-face {
        font-family: 'MontserratBold';
        src:url(${MontserratBold});
    }
    @font-face {
        font-family: 'MontserratSemiBold';
        src:url(${MontserratSemiBold});
    }
    @font-face {
        font-family: 'MontserratRegular';
        src:url(${MontserratRegular});
    }
    @font-face {
        font-family: 'MontserratLight';
        src:url(${MontserratLight});
    }
`;